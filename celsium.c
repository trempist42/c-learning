/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   celsium.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/10 04:41:50 by rafalmer          #+#    #+#             */
/*   Updated: 2018/11/10 04:41:52 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		main(void)
{
	double	fahr;
	double	cels;
	int		lower;
	int		upper;
	int		step;

	lower = 0;
	upper = 300;
	step = 20;

	cels = lower;
	while (cels <= upper)
	{
		fahr = (cels * 9 / 5) + 32;
		printf("%.2f\t%.2f\n", cels, fahr);
		cels = cels + step;
	}
}
