/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fahrenheit.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/10 04:41:59 by rafalmer          #+#    #+#             */
/*   Updated: 2018/11/10 04:42:01 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		main(void)
{
	double	fahr;
	double	cels;
	int		lower;
	int		upper;
	int		step;

	lower = 0;
	upper = 300;
	step = 20;

	fahr = lower;
	while (fahr <= upper)
	{
		cels = 5 * (fahr - 32) / 9;
		printf("%.2f\t%.2f\n", fahr, cels);
		fahr = fahr + step;
	}
}
